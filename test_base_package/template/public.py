# -*- coding: utf-8 -*-
pytest_template = """[pytest]
base_url = 
addopts = -vs --alluredir ./tmp/allure_results --clean-alluredir
testpaths = ./cases
python_files = test_*.py
python_classes = Test*
"""

runner_template = """# -*- coding: utf-8 -*-
import os
import pytest

if __name__ == '__main__':
    # 更新测试报告
    pytest.main(['-s', '-q', '--clean-alluredir', '--alluredir=allure-results'])
    os.system(r"allure generate -c -o ./allure-report")
"""
